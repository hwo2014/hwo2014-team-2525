#include "game_logic.h"
#include "protocol.h"
#include <cmath>

using namespace hwo_protocol;

game_logic::game_logic()
	: action_map
		{
			{ "join", &game_logic::on_join },
			{ "gameStart", &game_logic::on_game_start },
			{ "carPositions", &game_logic::on_car_positions },
			{ "crash", &game_logic::on_crash },
			{ "gameEnd", &game_logic::on_game_end },
			{ "error", &game_logic::on_error },
			{ "gameInit", &game_logic::on_game_init },
			{ "yourCar", &game_logic::on_your_car },
			{ "lapFinished", &game_logic::on_lap_finished },
			{ "turboAvailable", &game_logic::on_turbo_available},
			{ "turboStart", &game_logic::on_turbo_start},
			{ "turboEnd", &game_logic::on_turbo_end},
			{ "spawn", &game_logic::on_spawn },
			{ "finish", &game_logic::on_finish},
			{ "tournamentEnd", &game_logic::on_tournament_end}
		}
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find(msg_type);
	if (action_it != action_map.end())
		return (action_it->second)(this, data);
	else
	{
		std::cout << "ERR :: Unknown message type: " << msg_type << std::endl;
		return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	std::cout << "INF :: Joined" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	std::cout << "RUN :: Race started" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	int option = 0;
	int switched = -1;

	for( int i = 0; i < data.size(); i++)
	{
		if(data[i]["id"]["color"].as<std::string>() == car["color"].as<std::string>())
		{
			int index = data[i]["piecePosition"]["pieceIndex"].as<int>();
			int next_index = (index+1)%track.size();
			int next_next_index = (index+2)%track.size();

			double car_angle = abs(data[i]["angle"].as<double>());
			if(track[index].has_member("lenght"))
				throttle = .9;
			else
				throttle = .3 + .6 * (1 - car_angle/45)*(1 - car_angle/45);

			if(track[next_index].has_member("angle"))
			{
				double radius = track[next_index]["radius"].as<double>();
				double angle = track[next_index]["angle"].as<double>();
				if(track[next_next_index].has_member("angle") && (track[next_next_index]["angle"].as<double>() * track[next_index]["angle"].as<double>() < 0))
					throttle /= 1.5;
				else
					throttle = fmin(1., (1- (angle / 360)) * (radius / 300));
			}

			if(is_turbo_available)
				if((track[index].has_member("length") && track[index]["length"].as<int>() >= 200) ||
					 (track[index].has_member("length") && track[next_index].has_member("length") &&
					 (track[index]["length"].as<int>() + track[next_index]["length"].as<int>()) >= 200 ))
					option = 3;

			if(track[index].has_member("switch"))
			{
				int car_lane = track.data["lanes"][data[i]["piecePosition"]["lane"]["startLaneIndex"].as<int>()]["distanceFromCenter"].as<int>();
				int next_turn_index = next_index;
				while(!track[next_turn_index].has_member("angle"))
					next_turn_index=(next_turn_index+1)%track.size();
				if(switched < 0 || switched != index)
				{
					if(track[next_turn_index]["angle"].as<double>() < 0. && car_lane >= 0)
					{
						option = 2;
						switched = index;
					}
					else
						if(track[next_turn_index]["angle"].as<double>() > 0. && car_lane <= 0)
						{
							option = 3;
							switched = index;
						}
				}
			}

			break;
		}
	}
	switch(option)
	{
		case 0: return { make_throttle(throttle) };
		case 1: return { make_turbo() };
		case 2: return { make_switchLane("Left") };
		case 3: return { make_switchLane("Right") };
		default: return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data) {
	is_turbo_available = true;
	std::cout << "TRB :: Turbo ready. Duration " << data["turboDurationMilliseconds"].as<int>() << "ms. Duration " << data["turboDurationTicks"].as<int>() << "ticks. Multipl " << data["turboFactor"] << "x" << std::endl;
	return { make_ping() };
}
game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data) {
	is_turbo_available = false;
	std::cout << "TRB :: Turbo activated" << std::endl;
	return { make_ping() };
}
game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data) {
	
	std::cout << "TRB :: Turbo ended" << std::endl;
	return { make_throttle(throttle/2) };
}
game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data) {
	std::cout << "NEW :: Car respawned" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	if(data["color"].as<std::string>() == car["color"].as<std::string>())
		std::cout << "DTH :: Car Crashed!" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
	std::cout << "LAP :: Finished lap" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "END :: Race ended" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "ERR :: Error: " << data.to_string() << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	std::cout << "INF :: Race init! Reading race description..." << std::endl;
	track.init(data["race"]["track"]);
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	car = data;
	std::cout << "INF :: My car is: " << car.to_string() << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
	std::cout << "INF :: Race finished" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
	std::cout << "END :: Tournament ended" << std::endl;
	return { make_ping() };
}
