#ifndef HWO_TRACK_DATA_H
#define HWO_TRACK_DATA_H

#include<iostream>
#include<jsoncons/json.hpp>

struct track_data
{
	jsoncons::json data;
	void init(const jsoncons::json& _data);
	jsoncons::json operator[](int index);
	double length(int index, int lane = -1);
	int size();
};

#endif //HWO_TRACK_DATA_H
