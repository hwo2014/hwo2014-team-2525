#include"track_data.h"
#include<cmath>

double abs(double x) { return (x<0)?-x:x; }

void track_data::init(const jsoncons::json& _data)
{
	data = _data;
}

jsoncons::json track_data::operator[](int index)
{
	return data["pieces"][index];
}

double track_data::length(int index, int lane)
{
	int offset = 0;
	if(lane != -1)
		offset = data["lanes"][lane]["distanceFromCenter"].as<int>();
	if((*this)[index].has_member("angle"))
		return abs((*this)[index]["angle"].as<double>())/180*M_PI
			* ((*this)[index]["radius"].as<double>())
			* (((*this)[index]["angle"].as<double>() > 0)?-1:1)*offset;
	else
		return ((*this)[index]["length"].as<double>());
}

int track_data::size()
{
	return data["pieces"].size();
}
